﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterWhichMono.Actor;
using Microsoft.Xna.Framework;

namespace WaterWhichMono
{
    interface IGameMediator
    {

        void Add(GameObject gameObject);

        void Add(ref GameObject gameObject);

        bool IsPlayerDead();

        Vector2 GetPlayerPosition();


    }
}

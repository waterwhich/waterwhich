﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace WaterWhichMono.Collision
{
    class Collision2D
    {
        

        /// <summary>
        ///  円と円の当たり判定
        /// </summary>
        /// <param name="p0">円1の中心点</param>
        /// <param name="p1">円2の中心点</param>
        /// <param name="r0">円1の半径</param>
        /// <param name="r1">円2の半径</param>
        /// <returns></returns>
        public static bool Circle_Circle(Vector2 p0, Vector2 p1, float r0, float r1)
        {

            float c = Vector_Three_Squares(p0, p1);

            if (c <= (r0 + r1))
            {

                return true;

            }

            return false;
        }

        /// <summary>
        /// 三平方の定理で2つの点の中心の距離を図る
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public static float Vector_Three_Squares(Vector2 p0, Vector2 p1,ref float x,ref float y)
        {
             x = p0.X - p1.X;
             y = p0.Y - p1.Y;

            float c2 = (x * x) + (y * y);

            float c = (float)Math.Sqrt((double)c2);

            return c;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static float Vector_Three_Squares(Vector2 p0, Vector2 p1)
        {
            float x = p0.X - p1.X;
            float y = p0.Y - p1.Y;

            float c2 = (x * x) + (y * y);

            float c = (float)Math.Sqrt((double)c2);

            return c;
        }

        /// <summary>
        /// 泡と弾の衝突判定
        /// </summary>
        /// <param name="p0">泡</param>
        /// <param name="p1">弾</param>
        /// <param name="foamSize">泡サイズ</param>
        /// <param name="bulletSize">弾サイズ</param>
        /// <param name="current">今、泡の表面に衝突したか</param>
        /// <returns></returns>
        public static bool Foam_Circle(Vector2 p0,Vector2 p1,float foamSize,float bulletSize,ref bool current)
        {

            bool previos = current;

            current = Circle_Circle(p0, p1, foamSize, bulletSize);

            if (current)
            {
                return false;
            }
            else
            {
                if (previos) return true;
            }

            return false;

        }




    }
}

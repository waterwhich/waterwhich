﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using WaterWhichMono.Device;
using WaterWhichMono.Actor;
using WaterWhichMono.Def;

namespace WaterWhichMono.Scene
{
    /// <summary>
    /// ゲームプレイクラス
    /// </summary>
    class GamePlay : IScene
    {
        private bool isEndFlag;//終了フラグ
        private GameObjectManager manager;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public GamePlay(GameDevice gameDevice)
        {

            manager = new GameObjectManager();
            GameObject greatBullet1 = new GreatBulletGraph(new Vector2(0, Screen.Height / 2),true,0.5f,0.5f,1f);
            GameObject greatBullet2 = new GreatBulletGraph(new Vector2(0, Screen.Height / 2),true,0.5f,-0.5f,1f);
            manager.Add(greatBullet1);
            manager.Add(greatBullet2);
            GameObject bullet = new PlayerBullet(new Vector2(Screen.Width / 2 - 100, Screen.Height / 2));
            //manager.Add(bullet);


        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer">描画オブジェクト</param>
        public void Draw(Renderer renderer)
        {
            renderer.DisableAntiAliasing();
            manager.Draw(renderer);

            renderer.End();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize()
        {
            //シーン終了フラグを初期化
            isEndFlag = false;
            


        }

        /// <summary>
        /// シーン終了か？
        /// </summary>
        /// <returns></returns>
        public bool IsEnd()
        {
            return isEndFlag;
        }

        /// <summary>
        /// 次のシーンは
        /// </summary>
        /// <returns>次のシーン名</returns>
        public Scene Next()
        {
            Scene nextScene = Scene.GamePlay;
            return nextScene;
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        public void Shutdown()
        {

            

        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="gameTime">ゲーム時間</param>
        public void Update(GameTime gameTime)
        {

            manager.Update(gameTime);

        }
    }
}

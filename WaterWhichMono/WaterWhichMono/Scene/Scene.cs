﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterWhichMono.Scene
{
    /// <summary>
    /// シーン名の列挙型
    /// </summary>
    enum Scene
    {
        Load,
        Title,
        GamePlay,
        Ending,
        GameOver,
        Result,
    }
}

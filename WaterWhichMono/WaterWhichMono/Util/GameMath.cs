﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;


namespace WaterWhichMono.Util
{
    class GameMath
    {

        /// <summary>
        /// 二つの点の距離の単位ベクトルの求め方
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public static Vector2 UnitVector(Vector2 p0,Vector2 p1)
        {

            float a = 0;
            float b = 0;
            float c = Collision.Collision2D.Vector_Three_Squares(p0, p1,ref a,ref b);

            Vector2 unitVector = new Vector2(a / c, b / c);

            return unitVector;

        }

        /// <summary>
        /// 回転移動の一次変換(回転移動)
        /// </summary>
        public static Vector2 Test(int a,Vector2 diff,Vector2 original)
        {
            Vector2 origin = original;
            Vector2 diffrence = diff;
              
            Vector2 x1 = new Vector2((float)Math.Cos( a *( Math.PI / 180)), -(float)Math.Sin(a * (Math.PI / 180)));
            Vector2 x2 = new Vector2((float)Math.Sin( a *(Math.PI / 180)), (float)Math.Cos(a * (Math.PI / 180)));

            x1.X *= diffrence.X;
            x1.Y *= diffrence.Y;

            x2.X *= diffrence.X;
            x2.Y *= diffrence.Y;

            Vector2 result = new Vector2(x1.X + x1.Y,x2.X + x2.Y);

            return result + origin;

        }

        
        /// <summary>
        /// その方角に向けて移動
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector2 TestVelocity(int a)
        {
           Vector2 c = Test(a, Vector2.UnitX, Vector2.Zero);

            return c;

        }

        public static Vector2 TriGraph(float a,float b)
        {
            
           

           Vector2 vlo = new Vector2(a,b *  ((float)Math.Sin((double)a * (Math.PI / 180))));

            return vlo;




        }
        public static Vector2 TriGraphC(float a, float b)
        {



            Vector2 vlo = new Vector2(a, b * ((float)Math.Cos((double)a * (Math.PI / 180))));

            return vlo;




        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterWhichMono.Def
{
    /// <summary>
    /// 静的画面サイズクラス
    /// </summary>
    static class Screen
    {
        //C#
        public static readonly int Width = 640;//画面横幅
        public static readonly int Height = 480;//画面縦幅
    }
}

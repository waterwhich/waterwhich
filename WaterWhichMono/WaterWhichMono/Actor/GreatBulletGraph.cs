﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using WaterWhichMono.Util;

namespace WaterWhichMono.Actor
{
    class GreatBulletGraph : GameObject
    {

        int count;
        private Vector2 pre;
        bool sinorcos;
        float ysize;
        float speed;
        float x;
        public GreatBulletGraph(Vector2 position, bool sinorcos,float x, float ysize, float speed, int scale = 1, int fream = 0, GameObjectTag tag = GameObjectTag.Enemy, string spriteName = "GreatBullet1", string name = "GreatBullet1") : base(position, scale, fream, tag, spriteName, name)
        {

            this.speed = speed;
            this.sinorcos = sinorcos;
            this.ysize = ysize;
            this.x = x;
        }

        public override bool Hit(GameObject obj)
        {
            return false;
        }

        public override bool IsDead()
        {
            return base.IsDead();
        }

        public override void Update(GameTime gameTime)
        {

            pre = new Vector2(x, GameMath.TriGraph(count, ysize).Y);
            count++;
            if (sinorcos)
            {
               //Vector2 velo = GameMath.UnitVector(transform.position, transform.position - new Vector2(1f, GameMath.TriGraph(count, ysize).Y));
                transform.position += (new Vector2(x, GameMath.TriGraph(count, ysize).Y) * speed);
                //transform.position += velo ;
            }
            else
            {
                transform.position += new Vector2(x, GameMath.TriGraphC(count, ysize).Y) * speed; 
            }
        }
    }
}

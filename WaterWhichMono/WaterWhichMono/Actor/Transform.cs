﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
namespace WaterWhichMono.Actor
{
    class Transform : ICloneable
    {

        private Vector2 vector2D;
        private float scale;
        public Vector2 position
        {
            get
            {
                return vector2D;
            }
            set
            {
                vector2D = value;
            }
        }

        public float Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        public object Clone()
        {
            Transform transform = new Transform();
                transform.position = position;
            transform.Scale = scale;
            return transform;
        }
    }
}

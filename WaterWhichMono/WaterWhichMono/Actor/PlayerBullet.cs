﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterWhichMono.Device;
using Microsoft.Xna.Framework;
namespace WaterWhichMono.Actor
{
    class PlayerBullet : GameObject
    {
        
        private Vector2 velocity = new Vector2(1, 0);
        public PlayerBullet(Vector2 position, int scale = 1, int fream = 0, GameObjectTag tag = GameObjectTag.PlayerBullet, string spriteName = "NomalBullet") : base(position, scale, fream, tag, spriteName)
        {

            sprite = new Sprite(transform.position, "NomalBullet", 32, 32, 0);

        }

        public override bool Hit(GameObject obj)
        {
            return false;
        }

        public override bool IsDead()
        {
            return base.IsDead();
        }

        public override void Update(GameTime gameTime)
        {
            transform.position += velocity;
        }
    }
}

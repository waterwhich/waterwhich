﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace WaterWhichMono.Actor
{
    class Bullet : GameObject
    {

        private bool inFoam;
        private Vector2 velocity;
        public Bullet(Vector2 position,Vector2 velocity,int scale = 1,int fream = 0, GameObjectTag tag = GameObjectTag.Bullet, string spriteName = "NomalBullet") : base(position, scale,fream,tag, spriteName)
        {

            this.velocity = velocity;
            sprite = new Sprite(transform.position,"NomalBullet", 32, 32, 0);

        }

        public override GameObject Clone()
        {
            return new Bullet(transform.position, velocity);
        }

        public override bool Hit(GameObject obj)
        {
            if(
            Collision.Collision2D.Foam_Circle(obj.Transform.position, Transform.position, obj.Transform.Scale, transform.Scale, ref inFoam)
            ){
                velocity *= -1;
                return true;
            }
            return false;
        }

        public override bool IsDead()
        {
            return isDead;
        }

        public override void Update(GameTime gameTime)
        {
            transform.position += velocity;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace WaterWhichMono.Actor
{
    class Player : GameObject
    {
        public Player(Vector2 position, int scale,int fream,GameObjectTag tag = GameObjectTag.Player, string spriteName = "Player", string name = null) : base(position,scale,fream,tag, spriteName, name)
        {
        }

        public override void Draw()
        {
            base.Draw();
        }

        public override bool Hit(GameObject obj)
        {
            return false;
        }

        public override bool IsDead()
        {
           return base.IsDead();
        }

        public override void Update(GameTime gameTime)
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace WaterWhichMono.Actor
{


    /// <summary>
    /// ランプの敵が出す炎の玉を撃つ3つ分の弾を少し時間をずらして撃ち波線に撃つプレイヤーに向かっては撃たない
    /// </summary>
    class NomalEnemyShot1 : ShotSystem
    {

        int i = 0;
        float y = 1;

        public NomalEnemyShot1(IGameMediator mediator, Transform transform) : base(mediator, transform)
        {

            freams = new int[4];
            freams[0] = 10;//画面上方向
            freams[1] = 10;//真ん中なんです
            freams[2] = 10;//画面したっー
            freams[3] = 60;//休憩時間なのです

        }

        public override void Update(GameTime gameTime)
        {
            i++;

            if (i == freams[count])
            {
                i = 0;
                //スイッチで3つの弾パターンと1つの弾を撃たないパターンを追加した
                switch (count) {

                    case 0:
                    mediator.Add(new GreatBullet(transform.position,new Vector2(2,-0.2f * y), mediator));//上方向
                        count++;
                        break;

                    case 1:
                        mediator.Add(new GreatBullet(transform.position, new Vector2(2, 0 * y), mediator));//直線ショット
                        count++;
                        break;

                    case 2:
                        mediator.Add(new GreatBullet(transform.position, new Vector2(2, 0.2f * y), mediator));//下方向
                        count++;
                        break;
                    case 3: //休憩タイムです
                        y *= -1;
                        count = 0;
                        break;
                }
        }

            
        }
    }
}

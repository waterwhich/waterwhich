﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using WaterWhichMono.Device;
using WaterWhichMono.Scene;
namespace WaterWhichMono.Actor
{

    delegate bool Add( GameObject obj);
    delegate bool AddRef(ref GameObject obj);

    class GameObjectManager : IGameMediator
    {

        

        List<GameObject> addObj;
        List<GameObject> bullets;
        List<GameObject> playerBullets;
        List<GameObject> foams;
        List<GameObject> enemys;
        List<GameObject> items;
        GameObject player;
        
        Add[] Adds;
        AddRef[] addRefs;

        public void Removes(GameObject obj)
        {
            addObj.RemoveAll(i => i == obj);
        }

        public void AddUpdate()
        {
            foreach (var j in addObj)
            {
                for (int i = 0; i < Adds.Length; i++)
                {
                    if (Adds[i](j)) continue;
                }
            }

            addObj.Clear();

            

        }

        public GameObjectManager()
        {

            bullets = new List<GameObject>();
            foams = new List<GameObject>();
            enemys = new List<GameObject>();
            items = new List<GameObject>();
            playerBullets = new List<GameObject>();
            addObj = new List<GameObject>();

            Adds = new Add[5];
            addRefs = new AddRef[5];
            Adds[0] = new Add(AddBullets);
            Adds[1] = new Add(AddFoams);
            Adds[2] = new Add(AddEnemy);
            Adds[3] = new Add(AddItem);
            Adds[4] = new Add(AddPlayerBullets);

            addRefs[0] = new AddRef(AddBullets);
            addRefs[1] = new AddRef(AddFoams);
            addRefs[2] = new AddRef(AddEnemy);
            addRefs[3] = new AddRef(AddItem);
            addRefs[4] = new AddRef(AddPlayerBullets);

        }

        public void Add(GameObject obj)
        {
            addObj.Add(obj);

        }
        #region 追加系

        bool AddPlayerBullets(GameObject obj)
        {
            if(obj.Tag == GameObjectTag.PlayerBullet) {

                playerBullets.Add(obj);
                
                return true;
            }
            return false;
        }

        bool AddBullets(GameObject obj)
        {
            if(obj.Tag == GameObjectTag.Bullet) {

                bullets.Add(obj);
                
                return true;
            }
            return false;
        }
        bool AddFoams(GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Foam) {

                foams.Add(obj);
                return true;
            }
            return false;
        }
        bool AddEnemy(GameObject obj)
        {
            if(obj.Tag == GameObjectTag.Enemy)
            {
                enemys.Add(obj);
                return true;
            }
            return false;
        }
        bool AddItem(GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Item)
            {
                items.Add(obj);
                return true;
            }
            return false;
        }

        bool AddPlayer(GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Player)
            {
                player = obj;
                return true;
            }
            return false;
        }
        
        bool AddPlayerBullets(ref GameObject obj)
        {
            if (obj.Tag == GameObjectTag.PlayerBullet)
            {

                playerBullets.Add(obj);
                return true;
            }
            return false;
        }

        bool AddBullets(ref GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Bullet)
            {

                bullets.Add(obj);
                return true;
            }
            return false;
        }
        bool AddFoams(ref GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Foam)
            {

                foams.Add(obj);
                return true;
            }
            return false;
        }
        bool AddEnemy(ref GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Enemy)
            {
                enemys.Add(obj);
                return true;
            }
            return false;
        }
        bool AddItem(ref GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Item)
            {
                items.Add(obj);
                return true;
            }
            return false;
        }

        bool AddPlayer(ref GameObject obj)
        {
            if (obj.Tag == GameObjectTag.Player)
            {
                player = obj;
                return true;
            }
            return false;
        }
        #endregion 追加系
        public void Update(GameTime gameTime)
        {

            UpdateObjs(gameTime);
            AddUpdate();

        }

        private void UpdateObjs(GameTime gameTime)
        {
            bullets.ForEach(i => i.Update(gameTime));
            enemys.ForEach(i => i.Update(gameTime));
            foams.ForEach(i => i.Update(gameTime));
            items.ForEach(i => i.Update(gameTime));
            playerBullets.ForEach(i => i.Update(gameTime));
            Foams_Bullets();
            Enemys_Bullets();
            
        }
        private void DrawObjs(Renderer renderer)
        {
            bullets.ForEach(i => i.Draw());
            enemys.ForEach(i => i.Draw());
            foams.ForEach(i => i.Draw());
            items.ForEach(i => i.Draw());
            playerBullets.ForEach(i => i.Draw());
        }

        private void Foams_Bullets()
        {

            foreach(var f in foams)
            {
                foreach(var b in bullets)
                {
                    b.Hit(f);
                }
            }

        }

        private void Enemys_Bullets()
        {
            foreach(var e in enemys)
            {
                foreach(var pb in playerBullets)
                {

                    e.Hit(pb);
                                        
                }
            }
        }

        public void Draw(Renderer renderer)
        {
            DrawObjs(renderer);
        }

        public void Remove()
        {

            playerBullets.RemoveAll(i => i.IsDead());
            enemys.RemoveAll(i => i.IsDead());
            items.RemoveAll(i => i.IsDead());
            enemys.RemoveAll(i => i.IsDead());
            bullets.RemoveAll(i => i.IsDead());
            foams.RemoveAll(i => i.IsDead());

        }

        public bool IsPlayerDead()
        {

            if (player.IsDead())
            {
                return true;
            }
            return false;

        }

        public Vector2 GetPlayerPosition()
        {

            return player.Transform.position;

        }

        public void Add(ref GameObject obj)
        {

            addObj.Add(obj);

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using WaterWhichMono.Util;
namespace WaterWhichMono.Actor
{
    class DebugEnemy : GameObject
    {

        private ShotSystem shotSystem;

        private Vector2 baseTransform;

        private int count;

        public DebugEnemy(Vector2 position, IGameMediator mediator,int scale = 32, int fream = 0, GameObjectTag tag = GameObjectTag.Enemy, string spriteName = "Enemy", string name = "Enemy") : base(position, scale, fream, tag, spriteName, name)
        {
            isDead = false;

            int[] a = new int[1];

            a[0] = 100;

            baseTransform = position;

            bool[] b = new bool[1];

            b[0] = true;
           

            shotSystem = new NomalEnemyShot1(mediator, transform);
        }

        public override bool Hit(GameObject obj)
        {
            if(Collision.Collision2D.Circle_Circle(transform.position,obj.Transform.position,transform.Scale / 2, obj.Transform.Scale / 2))
            {
                isDead = true;
                return true;
            }
            return false;
        }

        public override bool IsDead()
        {
            return isDead;
        }

        public  override void Update(GameTime gameTime)
        {

            shotSystem.Update(gameTime);
            /*
            if(count >= 360)
            {
                count = 0;
            }
            count++;
            transform.position = GameMath.Test(count, new Vector2(100, 0), baseTransform);
            */

            //transform.position += GameMath.TestVelocity(-40);

            count++;

            

            transform.position += new Vector2(1.0f, GameMath.TriGraphC(count, 1f).Y * 1);
            //Console.WriteLine(GameMath.TriGraph(count, ref i));
            
        }
        

        

    }
}

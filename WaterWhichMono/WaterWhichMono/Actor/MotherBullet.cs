﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace WaterWhichMono.Actor
{
    /// <summary>
    /// 弾を生み出す弾 生み出だした弾をこのクラスで管理できるようにする（例　この球が特定の場所に行ったら子供弾が一斉に動くなど
    /// </summary>
    class ParentBullet : ShotSystem
    {

        List<GameObject> children;
        GameObject child;
        int i = 0;
        public ParentBullet(IGameMediator mediator, Transform transform,int[] fream,GameObject child) : base(mediator, transform)
        {

            children = new List<GameObject>();
            this.child = child;
            this.freams = fream;

        }

        public override void Update(GameTime gameTime)
        {
            Sys();
        }

        public void Sys()
        {
            i++;
            if(i > freams[0])
            {
                i = 0;
                
                child.Transform.position = transform.position;
                children.Add(child.Clone());
                mediator.Add(children[count]);
                count++;
            }
        }
        /// <summary>
        /// もう使わない
        /// </summary>
        public void OldSys()
        {
            /*
            i++;
            if (i > freams[0])
            {
                for (int j = 0; j < children.Length; j++)
                {
                    child.Transform.position = transform.position;
                    children[j] = child.Clone();
                    mediator.Add(children[j]);
                }
                i = 0;
            }
            for (int x = 0; x < children.Length; x++)
            {
                if (children[x] == null) return;
                children[x].Transform.position = children[x].Transform.position;
            }
            */
        }
    }

}

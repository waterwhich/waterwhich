﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace WaterWhichMono.Actor
{
    class GreatBullet : GameObject
    {

        private ShotSystem shotSystem;
        private Vector2 velocity;
        public GreatBullet(Vector2 position, Vector2 vector2,IGameMediator mediator, int scale = 32, int fream = 0, GameObjectTag tag = GameObjectTag.Bullet, string spriteName = "GreatBullet1", string name = "GreatBullet1") : base(position, scale, fream, tag, spriteName, name)
        {
            isDead = false;
            this.velocity = vector2;
            int[] a = new int[3];

            a[0] = 100;
            a[1] = 10;
            a[2] = 10;



            bool[] b = new bool[3];

            b[0] = true;
            b[1] = true;
            b[2] = false;

            shotSystem = new ParentBullet(mediator, transform, a, new Bullet(transform.position, Vector2.Zero));
        }


       

        public override bool Hit(GameObject obj)
        {
            if (Collision.Collision2D.Circle_Circle(transform.position, obj.Transform.position, transform.Scale / 2, obj.Transform.Scale / 2))
            {
                isDead = true;
                return true;
            }
            return false;
        }

        public override bool IsDead()
        {
            return isDead;
        }

        public override void Update(GameTime gameTime)
        {
            shotSystem.Update(gameTime);
            transform.position += velocity;
        }



    }
}

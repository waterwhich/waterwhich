﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using WaterWhichMono.Device;
namespace WaterWhichMono.Actor
{
    class Foam : GameObject
    {
        public Foam(Vector2 position,int scale = 16,int fream = 0,GameObjectTag tag = GameObjectTag.Foam, string spriteName = "Foam") : base(position,scale,fream, tag, spriteName)
        {

            sprite = new Sprite(position,"Foam", 32, 32, 0);
            

        }

        public override void Draw()
        {
            sprite.Draw();
        }

        public override bool Hit(GameObject obj)
        {
            if(
            Collision.Collision2D.Circle_Circle(transform.position, obj.Transform.position, 32f, 5f)
            ){
                Console.WriteLine("Hit");
                return true;

            }
            return false;
        }

        public override bool IsDead()
        {
            return isDead;
        }

        public override string ToString()
        {
            return name;
        }

        public override void Update(GameTime gameTime)
        {
            sprite.Update(gameTime);
        }

        

    }
}

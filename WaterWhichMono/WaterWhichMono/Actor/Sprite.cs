﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;//Assert用
using Microsoft.Xna.Framework;
using WaterWhichMono.Util;
using WaterWhichMono.Device;
namespace WaterWhichMono.Actor
{
    class Sprite
    {

        private string spriteName;
        private Motion motion;
        private int spriteSizeX; //スプライト表示サイズX軸
        private int spriteSizeY; //スプライト表示サイズY軸
        private int motionSize; //アニメーションのフレーム数
        private Vector2 position;
        private Renderer renderer;
        public Sprite(Vector2 position,string name=null,int spriteSizeX = 64,int spriteSizeY = 64,int motionSize = 0)
        {

            this.position = position;
            spriteName = name;
            this.spriteSizeX = spriteSizeX;
            this.spriteSizeY = spriteSizeY;
            this.motionSize = motionSize;
            
            InitMotion();
            renderer = GameDevice.Instance().GetRenderer();

        }

        

        public string name
        {
            set
            {
                spriteName = value;
            }
            get
            {
                return spriteName;
            }
        }

        public void InitMotion(float time = 1.0f)
        {
            
            motion = new Motion(new Range(0,motionSize),new CountDownTimer(time));

            for(int i = 0;i <= motionSize; i++)
            {
                motion.Add(i, new Rectangle(i * spriteSizeX, i * spriteSizeY, spriteSizeX, spriteSizeY));
            }
        }

        public void Draw( )
        {

            renderer.DrawTexture(spriteName, position, motion.DrawingRange(0) ,1.0f);

        }

        public void AnimationDraw( )
        {
            Debug.Assert( motionSize != 0              
               ,"motionSizeが0ですね、アニメーションはできないよ！");
            renderer.DrawTexture(spriteName, position, motion.DrawingRange(), 1.0f);
        }

        public void Update(GameTime gameTime)
        {
            motion.Update(gameTime);
        }



    }
}

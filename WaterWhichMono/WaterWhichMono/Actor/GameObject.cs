﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using WaterWhichMono.Device;
using WaterWhichMono.Def;
using WaterWhichMono.Util;
namespace WaterWhichMono.Actor
{
    abstract class GameObject 
    {

        protected bool isDead;

        protected string name;

        private GameObjectTag tag;

        protected Transform transform = new Transform();

        protected Renderer render;

        protected Sprite sprite;      

        public GameObject(Vector2 position,int scale = 1,int fream = 0 , GameObjectTag tag = GameObjectTag.UnNameTag, string spriteName = null,string name = null)
        {

            sprite = new Sprite(position,spriteName,scale,scale,fream);

            render = GameDevice.Instance().GetRenderer();

            transform.position = position;

            transform.Scale = scale;

            this.tag = tag;

            sprite.name = spriteName;

            name = spriteName;

        }

        

        public abstract void Update(GameTime gameTime);

        public virtual void Draw()
        {
            render.DrawTexture(sprite.name, transform.position);
        }
        
        public GameObjectTag Tag
        {
            get
            {
                return tag;
            }
            set
            {
                tag = value;
            }
        }

        public abstract bool Hit(GameObject obj);

        public Transform Transform
        {
            get { return transform; }
            set { transform = value; }
        }

        public virtual bool IsDead()
        {
            return isDead;
        }

        public virtual GameObject Clone()
        {
            throw new NotImplementedException();
        }
    }
}

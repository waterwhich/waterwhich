﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
namespace WaterWhichMono.Actor
{
    /// <summary>
    /// 自機狙い　
    /// </summary>
    class AimToPlayer : ShotSystem
    {
        //大きさ1の

        private int i;
        private float speed;
        public AimToPlayer(IGameMediator mediator, Transform transform,float speed,int[] fream,bool[] doshot) : base(mediator, transform)
        {
            i = 0;
            this.Doshot = doshot;
            this.freams = fream;
            this.speed = speed;
            count = 0;
        }

        public override void Update(GameTime gameTime)
        {

            i++;
            if (i < freams[count]) return;
            i = 0;
            count++;
            if (count == freams.Length)
            {
                count = 0;
            }
            if (!Doshot[count]) return;
            Vector2 velocity = Util.GameMath.UnitVector(new Vector2(0,0), transform.position);
            mediator.Add(new Bullet(transform.position, velocity * speed));
            

        }


    }
}
